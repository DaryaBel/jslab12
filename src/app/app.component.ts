import { Component, OnInit } from '@angular/core';
import { Person } from './shared/models/person.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'project3';
  persons: Person[] = [];
  ngOnInit() {
    for (let i=0; i<10; i++){
      this.persons.push(new Person(i, 'Иван', 'Иванов'));
    }
  }
  changePersons(person){
    this.persons.splice(this.persons.findIndex(human => human.id == person.id), 1, person);
  }
  deletePersons(id){
    this.persons.splice(this.persons.findIndex(human => human.id == id), 1);
  }
  addPersons(obj){
    let newId;
    if (this.persons.length!=0){
      newId = +this.persons[this.persons.length-1].id +1;
     }
     else {newId=1;}
    this.persons.push(new Person(newId, obj.name_man, obj.surname_man));
  }
}
